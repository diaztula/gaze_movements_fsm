# README #

Source code for Finite State Machine **(FSM)** gaze events classification. FSM was initially designed to be used in Linux environments. Though it would possible to use it Windows as well, since it depends on native Python libraries only (such as Numpy), we have never tested it. We are planning to distribute FSM with [ANACONDA](https://www.continuum.io/).

## Structure ##

The folder **gazeEventsDetection** contains the source of FSM. The file *gazeEvents.py* is the core of FSM that should be included by your scripts (see examples below). The other files inside gazeEventsDetection implement filters that are used to compute gaze events.

Pay special attention to the file *constants.py*. It defines some constants values that define the performance of FSM. The more important constants are:
* TRACKER_FREQ: frequency of the eye tracker (e.g. 30, 120, 500, ...)

* ONE_DEGREE: number of pixels that correspond to one degree of visual angle. This is needed to compute eye gaze velocity in degrees/seconds. Computing this value includes knowing the distance between the eyes and the monitor in cm, and also the resolution in pixels of the monitor and its size in cm.

* SMOOTH_X_Y_LEN: length of the window to filter X and Y-coordinates. For low-frequency eye trackers (30-60Hz) better to use 3, to avoid longer latencies. For high-end eye trackers (500-1000Hz) you may define a larger size to improve smoothing.

* SMOOTH_VEL_LEN: length of the window to filter gaze velocity samples. For low-end eye trackers (30-60Hz) better to use 3, to avoid longer latencies. For high-end eye trackers (500-1000Hz) you may define a larger size to improve smoothing.

* FRAME_DELAY: The idea of frame delay is to remove small noise by introducing a short latency. A frame delay of size N would permit to correct/interpolate a sequence of N-1 undefined samples. For example, for N = 3, the sequences (1 = def, 0 = undef):  
1 1 1 1 0 1  
1 1 1 0 0 1  
could be interpolated

* MIN_FIX_DUR: minimum fixation duration, usually set this value to 50-100 ms.

* MAX_FIX_VAR: minimum fixation variance (in degrees of visual angle). Our tests have shown that 2 works well for most users.

## Examples ##

The folder *examples* contains some examples that show how to use FSM. We defined also a C++ wrapper to use FSM in a C++ application.

## Eye tracking API ##

The folder *EyeTracker* contains scripts that read gaze samples from either SMI RED eye trackers or also emulate gaze position with the mouse. It is useful to test FSM and is used in the examples.


## TODO ##

* Compute eye tracker frequency automatically from gaze samples.

If you have any comments, doubts, or if you encounter a problem, please contact [Antonio Diaz Tula](mailto:diaztula@ime.usp.br)

Enjoy it!