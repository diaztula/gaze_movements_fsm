# -*- coding: utf-8 -*-
from math import floor, ceil

# Eye tracker frequency (number of samples per second). This might be computed automatically.
TRACKER_FREQ = 30   # Hz - eye tracker frequency

# Conversion factor between pixels and degrees. If you are not sure, left its default (40)
ONE_DEGREE          = 40 

# Eye tracker interval between samples in ms
TRACKER_STEP = int(ceil(1000.0/TRACKER_FREQ))

################# Filtering parameters ################
SMOOTH_X_Y_LEN = 3   # Windows size to smooth coordinates
SMOOTH_VEL_LEN  = 3   # Windows size to smooth velocity signal

# This is the N parameter, the size of the buffer.
BUFFER_SIZE = int(ceil(1000.0/TRACKER_STEP)) # number of recent samples to keep 1 second of data 
MIN_FIX_DUR = 50    # Minimum fixation duration (in ms)
MAX_FIX_VAR = 2     # Maximum fixation variance X one degree

# Thresholds to detect invalid samples, like (0,0) eye coordinates or pupil diameter too small
COORD_EPS           = 1e-2
PUPIL_EPS           = 10e-1

# Number of samples to LEARN noise level to detect saccades
NOISE_LEVEL_LEN     = int(ceil(60.0/TRACKER_STEP))  # About 60 ms

MIN_BLINK_LEN = 100      # ms
MAX_BLINK_LEN = 1000     # ms

# The idea of frame delay is to remove small noise by introducing a short latency
# A frame delay of size N would permit to correct/interpolate a sequence of N-1 undef samples.
# For example, for N = 3, the sequences (1 = def, 0 = undef):
## 1 1 1 1 0 1
## 1 1 1 0 0 1
# could be interpolated
FRAME_DELAY = 2

# Saccade detection is somehow tricky. Please see the README for details about each parameter 
MIN_VEL_TH          = 25
MAX_VEL_TH          = 65
SACC_ONSET_COEF     = 1.7 # Coefficient to determine saccade onset = MEAN + SACC_ONSET_COEFF * STD. 
SACC_OFFSET_COEF    = 0.4   # Coefficient to determine saccade offset = THRESHOLD_ONSET * SACC_OFFSET_COEFF
MIN_SACC_ONSET_DUR  = 6     # Minimum duration of saccade onset to go to saccade
MIN_SACC_ONSET_DIST = 0.5 * ONE_DEGREE # Minimum distance of saccade onset to go to saccade
MAX_SACCADE_OFFSET_DURATION = 10 # Max saccade offset duration 

