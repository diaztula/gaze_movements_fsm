import math, numpy as np
import LinearFilter, DiffFilter, InterpolateFilter, NormalDist, GazeSample, GazeDataReader
from constants import *
from collections import deque

import warnings
warnings.filterwarnings("error")

# STATES
FIXATION    = 0
SACCADE     = 1
DRIFT       = 2
NOISE       = 3
BLINK       = 4
LOST        = 5
UNDEFINED   = 6
DEFINED     = 7

from EyeGazeState import EyeGazeState

class GazeEvents(object):
    def __init__(self):
        self.linfilt        = LinearFilter.LinearFilter()
        self.diffilt        = DiffFilter.DiffFilter()
        self.interpol       = InterpolateFilter.InterpolateFilter()
        self.velNoise       = NormalDist.NormalDist(coef=SACC_ONSET_COEF)
        self.velThreshold   = -1
        self.rawQ        = deque(maxlen=BUFFER_SIZE)
        self.smoothQ_X   = deque(maxlen=BUFFER_SIZE)
        self.smoothQ_Y   = deque(maxlen=BUFFER_SIZE)
        self.smoothQ_T   = deque(maxlen=BUFFER_SIZE)
        self.velocityQ   = deque(maxlen=BUFFER_SIZE)
        # ---------------------------------------
        self.isSaccade   = 0
        self.isSaccOffset= 0
        self.isSaccOnset = 0
        self.isFixation  = 0
        self.isDrift     = 0
        self.t0          = 0
        # ---------------------------------------
        self.frame_count   = 0
        self.current_state = EyeGazeState(LOST, self.frame_count)
        self.last_state    = EyeGazeState(LOST, self.frame_count)
        self.noise_count   = 0
        self.blink_count   = 0
        # ---------------------------------------

    def clearGazeEvents(self):
        """Clear all gaze events.
        """
        self.isSaccade = self.isFixation = self.isDrift = self.isSaccOffset = 0

    def addSample(self, t, xl, yl, pl, xr, yr, pr):
        """Add a new sample to the list.
        """
        sample = GazeSample.GazeSample(t, xl, yl, pl, xr, yr, pr)
        self._addSample(sample)

    def _addSample(self, sample):
        """Adds a new sample, updates gaze state.
        """
        self.defined, self.x, self.y, self.t = self.interpol.addSample(sample)
        if self.defined: 
            # We have a defined sample!!!
            self.rawQ.append(GazeSample.TimePoint(self.t, self.x, self.y))    
            # Enqueue raw sample
            self.smoothX, self.smoothY = self.linfilt.addSample(self.x, self.y) 
            # Do we have smoothed sample???
            if self.smoothX != -1 and self.smoothY != -1:     
                # Yes, we have smoothed sample!!!
                self.smoothQ_X.append(self.smoothX) # Enqueue smoothed coordinates 
                self.smoothQ_Y.append(self.smoothY) 
                self.smoothQ_T.append(self.t) 
                self.vel = self.diffilt.addSample(self.smoothX, self.smoothY, self.t) # Compute velocity
                # Do we have velocity?
                if self.vel != -1: # Yes, we have velocity!!!
                    self.velocityQ.append(self.vel)
                else:
                    self.defined = 0
            else:
                self.defined = 0
        # Updates statechart
        self.updateState()
        self.frame_count += 1   

    def updateState(self):
        """Updates the state chart according to the current state and the gaze, that can be FIXATION, SACCADE, DRIFT, LOST, NOISE, and BLINK.
        """
        # --------------------------- LOST -----------------------------------
        if self.current_state.type == LOST:
            if not self.defined: 
                self.current_state.len = self.t - self.current_state.ini
            else:               
                self.current_state.end = self.t
                self.last_state.setFromState(self.current_state)
                self.current_state.set(st=DRIFT, ini=self.t, x=self.smoothX, y=self.smoothY)
            
        # --------------------------- BLINK ----------------------------------
        elif self.current_state.type == BLINK:
            if not self.defined:                
                self.current_state.len = self.t - self.current_state.ini
                if self.current_state.len > MAX_BLINK_LEN:  ## blink becomes lost
                    self.current_state.set(st=LOST, ini=self.t, x = 0,  y = 0)
            else:
                self.current_state.end = self.t
                ## WE GO BACK TO THE PREV STATE, EXCEPT FOR SACCADES -- see state machine
                lastType   = self.last_state.type
                lastIni    = self.last_state.ini
                newLen     = self.last_state.len + self.current_state.len
                lastOnset  = self.last_state.isSaccOnset
                lastOffset = self.last_state.isSaccOffset
                self.last_state.setFromState(self.current_state)
                if lastType == SACCADE:   
                    self.current_state.set(st=DRIFT, ini=self.t, x=self.smoothX, y=self.smoothY)
                else: 
                    self.current_state.set(st=lastType, ini=lastIni, length=newLen, x=self.smoothX, y=self.smoothY, isSaccOnset=lastOnset, isSaccOffset=lastOffset)
    
        # --------------------------- NOISE -----------------------------------
        elif self.current_state.type == NOISE:
            if not self.defined:
                self.current_state.len = self.t - self.current_state.ini
                if self.current_state.len > MIN_BLINK_LEN:
                    self.current_state.set(st=BLINK, ini=self.t, x = 0,  y = 0)
                    self.blink_count += 1
            else:
                self.current_state.end = self.t
                lastType   = self.last_state.type
                lastIni    = self.last_state.ini
                newLen     = self.last_state.len + self.current_state.len
                lastOnset  = self.last_state.isSaccOnset
                lastOffset = self.last_state.isSaccOffset
                self.last_state.setFromState(self.current_state)
                self.current_state.set(st=lastType, ini=lastIni, length=newLen, x=self.smoothX, y=self.smoothY, isSaccOnset=lastOnset, isSaccOffset=lastOffset)
        else:
            # Current state is defined, so first of all we update current gaze event (fixation, saccade or drift?)
            # # # self.updateGazeEvent()
                
            # --------------------------- DRIFT -----------------------------------
            if self.current_state.type == DRIFT:
                if not self.defined:
                    self.current_state.end = self.t
                    self.last_state.setFromState(self.current_state)
                    self.current_state.set(st=NOISE, ini=self.t, isSaccOnset=self.isSaccOnset, isSaccOffset=self.isSaccOffset)
                    self.isSaccOnset = 0
                else: # Defined, update statechart
                    # Saccade onset?
                    self.velThreshold = self.velNoise.getThreshold()
                    if self.vel >= self.velThreshold:
                        # NOTE: WE DO NOT ADD THESE SAMPLES TO THE NOISE QUEUE
                        if not self.isSaccOnset: # First time going to sacc onset
                            self.isSaccOnset = 1
                            self.saccOnsetStart = self.t
                            self.saccOnset_x    = self.smoothX
                            self.saccOnset_y    = self.smoothY
                            self.saccOnsetFrame = self.frame_count
                        else: # Already detecting saccade onset, check if goes to saccade
                            if (self.t - self.saccOnsetStart) >= MIN_SACC_ONSET_DUR and math.sqrt((self.smoothX - self.saccOnset_x)**2 + (self.smoothY - self.saccOnset_y)**2) >= MIN_SACC_ONSET_DIST:
                                # Creates saccade
                                self.current_state.end = self.saccOnsetStart
                                self.current_state.len = self.current_state.end - self.current_state.ini
                                self.last_state.setFromState(self.current_state)
                                self.current_state.set(st=SACCADE, ini=self.saccOnsetStart, x=self.smoothX, y=self.smoothY)
                                print "Saccade"
                                self.isSaccOnset = 0 # limpando
                                self.isSaccOffset= 0
                                return # THIS RETURN HERE IS VERY IMPORTANT!!!!!!
                            else: # minimum duration/distance not fulfilled, stays...
                                pass 
                                # ADDITIONAL VALIDATION HERE MIGHT IMPROVE SACCADE ONSET DETECTION.....
                    else: # clean isSaccOnset
                        self.isSaccOnset = 0
                    # Saccade onset is a tendency, so it does not change current state
                    # If not saccade onset then check for a fixation
                    # First, add velocity to the velocity noise list
                    self.velNoise.addSample(self.vel)
                    n = int( math.ceil(MIN_FIX_DUR / (1000.0/TRACKER_FREQ)) ) # Number of samples to cover minimun fixation duration
                    # If last event was a saccade, then to compute dispersion we do not consider saccade points
                    # It means that after a saccade, the state will be drift until enough non-saccadic samples arrive and dispersion eventually is small
                    if len(self.smoothQ_X) >= n and (self.last_state.type != SACCADE or self.t >= self.last_state.end + MIN_FIX_DUR):
                        l       = len(self.smoothQ_X)-1
                        idx     = l
                        initial = max(0, l-n)
                        while idx >= initial and (self.t - self.smoothQ_T[idx]) <= MIN_FIX_DUR:
                            idx -= 1
                        idx += 1
                        lisX  = list(self.smoothQ_X)[idx:]
                        lisY  = list(self.smoothQ_Y)[idx:]
                        if len(lisX) > 1:
                            std  = (np.std(lisX, ddof=1) + np.std(lisY, ddof=1))/2
                            if std <= MAX_FIX_VAR*ONE_DEGREE: # We have a fixation!
                                fixX = np.median(lisX)
                                fixY = np.median(lisY)
                                self.current_state.end = self.t
                                self.last_state.setFromState(self.current_state)
                                self.current_state.set(st=FIXATION, ini=self.t, x=fixX, y=fixY)
                            else: # a drift...
                                self.current_state.len = self.t - self.current_state.ini
                                self.current_state.x = self.smoothX
                                self.current_state.y = self.smoothY                  
                        else: # a drift
                            self.current_state.len = self.t - self.current_state.ini
                            self.current_state.x = self.smoothX
                            self.current_state.y = self.smoothY                  
                    else: # a drift...
                        self.current_state.len = self.t - self.current_state.ini
                        self.current_state.x = self.smoothX
                        self.current_state.y = self.smoothY
                                                  
            # --------------------------- FIXATION -----------------------------------
            elif self.current_state.type == FIXATION:
                if not self.defined:
                    self.current_state.end = self.t
                    self.last_state.setFromState(self.current_state)
                    self.current_state.set(st=NOISE, ini=self.t, isSaccOnset=self.isSaccOnset, isSaccOffset=self.isSaccOffset)
                else: 
                    self.velThreshold = self.velNoise.getThreshold()
                    if self.vel >= self.velThreshold:
                        # NOTE: WE DO NOT ADD THESE SAMPLES TO THE NOISE QUEUE
                        if not self.isSaccOnset: # First time going to sacc onset
                            self.isSaccOnset = 1
                            self.saccOnsetStart = self.t
                            self.saccOnset_x    = self.smoothX
                            self.saccOnset_y    = self.smoothY
                            self.saccOnsetFrame = self.frame_count
                        else: # Already detecting saccade onset, check if goes to saccad
                            if (self.t - self.saccOnsetStart) >= MIN_SACC_ONSET_DUR and math.sqrt((self.smoothX - self.saccOnset_x)**2 + (self.smoothY - self.saccOnset_y)**2) >= MIN_SACC_ONSET_DIST:
                                self.current_state.end = self.saccOnsetStart
                                self.current_state.len = self.current_state.end - self.current_state.ini
                                self.last_state.setFromState(self.current_state)
                                print "Saccade"
                                self.current_state.set(st=SACCADE, ini=self.saccOnsetStart, x=self.smoothX, y=self.smoothY)
                                self.isSaccOnset = 0 # Cleaning
                                self.isSaccOffset= 0
                                return
                            else: # minimum duration/distance not fulfilled, stays...
                                pass 
                    else: # clean isSaccOnset
                        self.isSaccOnset = 0
                    # Saccade onset is a tendency, so it does not change current state
                    # IF not saccade onset, then check for a fixation
                    # Not a saccade, then it may be a fixation or a drift
                    # First, add velocity to the velocity noise list
                    self.velNoise.addSample(self.vel)
                    n = int( math.ceil(MIN_FIX_DUR / (1000.0/TRACKER_FREQ)) ) # Number of samples to cover minimun fixation duration
                    # If last event was a saccade, then to compute dispersion we do not consider saccade points
                    # It means that after a saccade, the state will be drift until enough non-saccadic samples arrive and dispersion eventually is small
                    if len(self.smoothQ_X) >= n and (self.last_state.type != SACCADE or self.t >= self.last_state.end + MIN_FIX_DUR):
                        l       = len(self.smoothQ_X)-1
                        idx     = l
                        initial = max(0, l-n)
                        while idx >= initial and (self.t - self.smoothQ_T[idx]) <= MIN_FIX_DUR:
                            idx -= 1
                        idx += 1
                        ### print "Diff t: ", self.t - self.smoothQ_T[idx]
                        lisX  = list(self.smoothQ_X)[idx:]
                        lisY  = list(self.smoothQ_Y)[idx:]
                        if len(lisX) > 1:
                            std  = (np.std(lisX, ddof=1) + np.std(lisY, ddof=1))/2
                            if std <= MAX_FIX_VAR*ONE_DEGREE: # We stilll have a fixation!
                                fixX = np.median(lisX)
                                fixY = np.median(lisY)
                                self.current_state.len = self.t - self.current_state.ini
                                self.current_state.x = fixX
                                self.current_state.y = fixY
                            else: # Go to drift
                                self.current_state.end = self.t
                                self.last_state.setFromState(self.current_state)
                                self.current_state.set(st=DRIFT, ini=self.t, x=self.smoothX, y=self.smoothY)
                        else: # Go to drift
                            self.current_state.end = self.t
                            self.last_state.setFromState(self.current_state) 
                            self.current_state.set(st=DRIFT, ini=self.t, x=self.smoothX, y=self.smoothY)
                    else: # Go to drift...
                        self.current_state.end = self.t
                        self.last_state.setFromState(self.current_state)
                        self.current_state.set(st=DRIFT, ini=self.t, x=self.smoothX, y=self.smoothY)
                
            # --------------------------- SACCADE -----------------------------------
            elif self.current_state.type == SACCADE:
                if not self.defined:
                    self.current_state.end = self.t
                    self.last_state.setFromState(self.current_state)
                    self.current_state.set(st=NOISE, ini=self.t, isSaccOnset=0, isSaccOffset=self.isSaccOffset)
                else: # State is defined, so lets implement the statechart
                    self.velThreshold = self.velNoise.getThreshold()
                    if self.vel >= self.velThreshold:
                        self.current_state.len = self.t - self.current_state.ini
                        self.current_state.x = self.smoothX
                        self.current_state.y = self.smoothY
                    elif self.vel >= SACC_OFFSET_COEF*self.velThreshold and (not self.isSaccOffset or \
                        (self.t-self.saccOffsetStart < MAX_SACCADE_OFFSET_DURATION and self.vel < self.velocityQ[-2])): # Detecting saccade offset
                        self.current_state.len = self.t - self.current_state.ini
                        self.current_state.x = self.smoothX
                        self.current_state.y = self.smoothY
                        if not self.isSaccOffset:   # Transition from saccade to saccade offset
                            self.isSaccOffset = 1
                            self.saccOffsetStart = self.t
                    else:
                        self.current_state.end = self.t
                        self.last_state.setFromState(self.current_state)
                        self.current_state.set(st=DRIFT, ini=self.t, x=self.smoothX, y=self.smoothY)

#============================================================
    
if __name__ == "__main__":    
    lastFrame   = 400
    count       = 0 

    # Antonio, detectar eventos do olhar
    gazeEvents = GazeEvents()
    
    # carregar e processar os dados do olhar
    raw_data =  GazeDataReader.GazeDataReader() # list of gaze samples
    raw_data.openFile("../SwitchTest/data/fastSaccades/eye.csv")
    
    sample = raw_data.getNextSample()
    while sample!= -1 and count < lastFrame:
        gazeEvents.addSample(sample)  #
        count += 1
        sample = raw_data.getNextSample()


