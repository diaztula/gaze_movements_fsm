# -*- coding: utf-8 -*-

from collections import deque
import numpy as np
from constants import *

class LinearFilter(object):
    def __init__(self):
        self.x = deque(maxlen=SMOOTH_X_Y_LEN)
        self.y = deque(maxlen=SMOOTH_X_Y_LEN)
        sigma  = 5.0
        ran = np.arange(0, SMOOTH_X_Y_LEN, 1.0, float)
        self.kernel = np.exp(- ( ((ran-SMOOTH_X_Y_LEN)**2) / (2*sigma**2)) )
        self.kernel /= np.sum(self.kernel)
        
    def addSample(self, x, y):
        self.x.append(x)
        self.y.append(y)
        if len(self.x) == SMOOTH_X_Y_LEN:
            fx = np.correlate(self.x, self.kernel, "valid")
            fy = np.correlate(self.y, self.kernel, "valid")
            return fx[0], fy[0]
        return -1, -1

if __name__ == "__main__":
    print "Testing linear filter"
    X = np.arange(200) + np.random.normal(scale=5.0, size=200)
    Y = np.arange(200) + np.random.normal(scale=5.0, size=200)
    import matplotlib.pyplot as plt
    plt.plot(X, Y, "r.-")
    FX = []
    FY = []
    filter = LinearFilter()
    for x, y in zip(X, Y):
        fx, fy = filter.addSample(x, y)
        if fx!=-1 and fy!=-1:
            FX.append(fx)
            FY.append(fy)
            
    plt.plot(FX, FY, "b.-")
    
    plt.show()
