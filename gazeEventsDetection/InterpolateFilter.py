# -*- coding: utf-8 -*-

from collections import deque
import numpy as np, sys
from GazeSample import *
from constants import *
from __builtin__ import True

class InterpolateFilter(object):
    def __init__(self):
        self.len            = 2*FRAME_DELAY-1
        self.samples        = deque(maxlen=self.len)
        self.valid          = deque(maxlen=self.len)
        self.defined        = False
        self.x = self.y = self.t = -1
    
    def validSample(self, sample):
        if  (abs(sample.left.x ) <= COORD_EPS and abs(sample.left.y ) <= COORD_EPS) or\
            (abs(sample.right.x) <= COORD_EPS and abs(sample.right.y) <= COORD_EPS) or\
            (abs(sample.left.pupil) <= PUPIL_EPS) or (abs(sample.right.pupil) <= PUPIL_EPS):
            return 0
        return 1

    def addSample(self, sample):
        self.samples.append(sample)
        self.valid.append(self.validSample(sample))
        if len(self.samples) < self.len: # Not enought samples, stays undefined
            self.defined = False
        else:
            if self.valid[self.len/2]: # Mid sample is valid, then the state is defined
                self.defined = True
                self.x = (self.samples[self.len/2].left.x + self.samples[self.len/2].right.x)/2
                self.y = (self.samples[self.len/2].left.y + self.samples[self.len/2].right.y)/2
                self.t = self.samples[self.len/2].tstamp
            else:
                # Mid sample is undefined, see if it is possible to interpolate
                count1 = np.sum(list(self.valid)[0:self.len/2 ])
                count2 = np.sum(list(self.valid)[self.len/2+1:])
                if count1 > 0 and count2 > 0:
                    count = self.x = self.y = 0
                    for i in range(self.len):
                        if self.valid[i]:
                            count += 1
                            self.x += (self.samples[i].left.x + self.samples[i].right.x)/2
                            self.y += (self.samples[i].left.y + self.samples[i].right.y)/2
                    self.x /= count
                    self.y /= count
                    self.t = self.samples[self.len/2].tstamp
                    self.defined = True
                else:
                    self.defined = False
                    self.t = self.samples[self.len/2].tstamp
                    self.x = self.y = 0
        return self.defined, self.x, self.y, self.t
              
    def isDefined(self):
        return self.defined, self.x, self.y, self.t
                
if __name__ == "__main__":
    print "Testing interpolate filter"
    import csv
    if len(sys.argv) == 2:
        f = file(sys.argv[1])
    else:
        f = file("../SwitchTest/data/fastSaccades/eye.csv")
    reader = csv.reader(f)
    filter = InterpolateFilter()
    count = 0
    X = []
    Y = []
    Z = []
    for d in reader:
        tstamp, leftx, lefty = int(float(d[0]))/10, int(float(d[1])), int(float(d[2]))
        leftpupil = float(d[3])
        rightx, righty = int(float(d[4])), int(float(d[5]))
        rightpupil = float(d[6])
        s = GazeSample(tstamp, leftx, lefty, leftpupil, rightx, righty, rightpupil)
        X.append(count)
        d, x, y, t = filter.addSample(s)
        if d:
            Y.append(0)
        else:
            Y.append(1)
        if filter.validSample(s):
            Z.append(5)
        else:
            Z.append(6)            
        count += 1
    import matplotlib.pyplot as plt
    plt.ylim(0, 10)
    plt.plot(X, Y, "r.-")
    plt.plot(X, Z, "b.-")
    plt.show()
    
    