import csv
from GazeSample import *

class GazeDataReader(object):
    def __init__(self, filePath="", header=False, divFactor=10):
        if filePath != "":
            self.openFile(filePath, header)
        self.c = 0
        self.divFactor = divFactor
    
    def openFile(self, filePath, header=False):
        try:
            self.file = open(filePath, "rt")
            self.reader = csv.reader(self.file)
            if header:
                self.reader.next()
            self.eof = False
            self.firstSample = True
        except:
            print "Unable to open file %s, no eye data file will be loaded"%(filePath)
            self.eof = True

    def getNextSample(self):
        if not self.eof:
            try:
                d = self.reader.next()
                tstamp, leftx, lefty = int(float(d[0]))/self.divFactor, int(float(d[1])), int(float(d[2]))
                if self.firstSample:
                    self.initTstamp = tstamp
                    print "Initial time stamp = ", tstamp
                    self.firstSample = False
                tstamp = tstamp - self.initTstamp
                leftpupil = float(d[3])
                rightx, righty = int(float(d[4])), int(float(d[5]))
                rightpupil = float(d[6])
                s = GazeSample(tstamp, leftx, lefty, leftpupil, rightx, righty, rightpupil)
            except:
                s = -1
                self.eof = True
        else:
            s = -1
        return s
