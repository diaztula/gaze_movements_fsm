class EyeGazeState:
    def __init__(self, st, ini=0, length=0):
        self.type = st
        self.ini = ini
        self.len = length
        self.end = 0
        self.x   = 0
        self.y   = 0
        self.isSaccOnset  = 1
        self.isSaccOffset = 1

    def set(self, st, ini=0, end=0, length=0, x=0, y=0, isSaccOnset=0, isSaccOffset=0):
        self.type  = st
        self.ini = ini
        self.end = end
        self.len = length
        self.x   = x
        self.y   = y
        self.isSaccOnset  = isSaccOnset
        self.isSaccOffset = isSaccOffset

    def setFromState(self, state):        
        self.type   = state.type
        self.ini    = state.ini
        self.len    = state.len
        self.end    = state.end
        self.x      = state.x
        self.y      = state.y 
        self.isSaccOnset  = state.isSaccOnset
        self.isSaccOffset = state.isSaccOffset

    def clone(self):
        new_st = EyeGazeState(self.type, self.ini, self.len)
        new_st.end = self.end
        new_st.x   = self.x
        new_st.y   = self.y
        new_st.isSaccOnset  = self.isSaccOnset
        new_st.isSaccOffset = self.isSaccOffset  
        return new_st

