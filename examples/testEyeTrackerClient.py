import sys
sys.path.append("../eyetracker/Client/")
import eyeTrackerClient as Client
import time

eyeTracker = Client.EyeTrackerClient()

eyeTracker.initEyeTracker()

eyeTracker.startEyeTracker()

for i in xrange(200):
    eyeData = eyeTracker.readDataFromEyeTracker()
    if len(eyeData) > 0:
        print "len=", len(eyeData), "  x=", eyeData[len(eyeData)-1].left.x, " y=", eyeData[len(eyeData)-1].left.y, " tstamp=", eyeData[len(eyeData)-1].left.tStamp
        #print "tstamp=%0.15f"%( eyeData[len(eyeData)-1].left.tStamp )
        # for d in eyeData:
        #    print "%i"%(d.left.tStamp)
    else:
        print "No data..."
    time.sleep(1.0/60.0)

eyeTracker.stopEyeTracker()
