# Examples #

1. Using *eyetracker* to read gaze samples from eye tracker.  
Take a look at *testEyeTrackerClient.py*. Before running this example, go to the folder **eyetracker/server** and run the bash script mouse.sh to run mouse emulation. Inside this script you can configure the simulated eye tracker frequency. The current point-of-gaze is emulated with the mouse pointer.  
Run the script *testEyeTrackerClient.py*. If everything is OK, it should print the current mouse position.

2. bla

If you have any comments, doubts, or if you encounter a problem, please contact [Antonio Diaz Tula](mailto:diaztula@ime.usp.br)

Enjoy it!
