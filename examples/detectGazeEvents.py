import sys, time
sys.path.append("../eyetracker/Client/")
import eyeTrackerClient as Client

sys.path.append("../gazeEventsDetection/")
from gazeEvents import *

# Object to detect gaze events
ge = GazeEvents()

# Object to read gaze samples
eyeTracker = Client.EyeTrackerClient()
eyeTracker.initEyeTracker()
eyeTracker.startEyeTracker()

lastState = None
try:
    while True:
        eyeData = eyeTracker.readDataFromEyeTracker()
        if len(eyeData) > 0:
            for sample in eyeData:
                ge.addSample(sample.left.tStamp, sample.left.x, sample.left.y, sample.left.pupilDiam, sample.right.x, sample.right.y, sample.right.pupilDiam)
        else:
            pass
        currentState = ge.current_state
        if lastState != currentState.type:
            print "New state: ", currentState.type
            lastState = currentState.type
        # Sleep for a while
        time.sleep(1.0/60.0)
except Exception as e:
    print "Some error: ", e
    print "Line: ", sys.exc_info()[2].tb_lineno
finally:
    eyeTracker.stopEyeTracker()

