import ctypes as c

class Eye(c.Structure):
    _fields_ = [("x", c.c_double), ("y", c.c_double), ("pupilDiam", c.c_double), ("tStamp", c.c_double)]
    
class BothEyes(c.Structure):    
    _fields_ = [("left", Eye), ("right", Eye)]

