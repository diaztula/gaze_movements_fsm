'''
Class to emulate an eye tracker with the mouse pointer
'''
import platform, time, math

osName = platform.system()
canReadMouse = False
if osName == "Windows":
    try: 
        import win32api
        canReadMouse = True
    except:
        print "Unable to load win32api, mouse emulation will not work"
elif osName == "Linux":
    try: 
        from Xlib import X, display
        disp = display.Display() 
        root = disp.screen().root
        canReadMouse = True
    except:
        print "Unable to load Xlib, mouse emulation will not work"
        print "Install it with sudo apt-get install python-xlib"

# Load eye structure
from EyeStruct import *

class EyeTracker():
    def __init__(self, binocular=False, scaleX=1.0, scaleY=1.0, calibPts=9, screen=None, fileName="", remoteIp="127.0.0.1", localIp="127.0.0.1", MAX_SAMPLE_SIZE = 500):    
        self.binocular = binocular
        self.scaleX    = scaleX
        self.scaleY    = scaleY
        print "Eye tracker emulator with the mouse"

    def createEyeTracker(self):
        if not canReadMouse:
            return 1
        return 0    
        
    def connectEyeTracker(self):
        return 0
                
    def calibrateEyeTracker(self):
        return 0
        
    def loadCalibration(self, calibFileName):
        return 0
    
    def saveCalibration(self, calibFileName):
        return 0

    def startEyeTracker(self):
        return 0
        
    def getEyeData(self):
        retCode = 1
        eyeData = None
        try:
            leftX, leftY = self.getMousePos()
            floatTime = (time.time())
            tStamp = c.c_double( math.floor(floatTime*1000) )
            # print tStamp, leftX, leftY
            leftPupil  = 5.0
            if self.binocular:
                eyeData = BothEyes(Eye(leftX*self.scaleX, leftY*self.scaleY, leftPupil, tStamp), Eye(leftX*self.scaleX, leftY*self.scaleY, leftPupil, tStamp))
            else: 
                eyeData = Eye(leftX, leftY, leftPupil, tStamp)
            retCode = 0
        except Exception as e:
            print "Exception while reading mouse position: ", e
        finally:
            return retCode, eyeData

    def stopEyeTracker(self):
        return 0
    
    def getMousePos(self):
        x, y = -1, -1
        if canReadMouse:
            if osName == "Linux":
                pointer = root.query_pointer()
                data = pointer._data
                x, y = data["root_x"], data["root_y"]
            elif osName == "Windows":
                x, y = win32api.GetCursorPos()
        return x, y
        
    def clearEyeTrackerData(self):
        self.eyeData.clear()
        return 0

