
from iViewXAPI import *           #iViewX library
from iViewXAPIReturnCodes import *
from ctypes import *

class SMI():
    def __init__(self, calibPts, screen):
        print "SMI create"
        self.screen          = screen
        self.calibPts        = calibPts
        self.systemData      = CSystem(0, 0, 0, 0, 0, 0, 0, 0)
        self.calibrationData = CCalibration(method=self.calibPts, visualization=1, displayDevice=self.screen, speed=0, autoAccept=0, foregroundBrightness=250, backgroundBrightness=30, targetShape=2, targetSize=25, targetFilename=b"")
        self.leftEye         = CEye(0,0,0)
        self.rightEye        = CEye(0,0,0)
        self.sampleData      = CSample(0,self.leftEye, self.rightEye,0)
        self.eventData       = CEvent('F', 'L', 0, 0, 0, 0, 0)
        self.accuracyData    = CAccuracy(0, 0, 0, 0)

    def connect(self, logFilePath, remoteIp, localIp):
        # ---------------------------------------------
        print "connect to iViewX"
        # ---------------------------------------------
        self.res = iViewXAPI.iV_SetLogger(c_int(1), c_char_p(logFilePath))
        self.res = iViewXAPI.iV_Connect(c_char_p(remoteIp), c_int(4444), c_char_p(localIp), c_int(5555))
        if self.res != 1:
            HandleError(self.res)
            return self.res
        
        self.res = iViewXAPI.iV_GetSystemInfo(byref(self.systemData))
        if self.res != 1:
            HandleError(self.res)
            return self.res

        print "iV_GetSystemInfo: "  + str(self.res)
        print "Samplerate: "        + str(self.systemData.samplerate)
        print "iViewX Version: "     + str(self.systemData.iV_MajorVersion)  + "." + str(self.systemData.iV_MinorVersion)  + "." + str(self.systemData.iV_Buildnumber)
        print "iViewX API Version: " + str(self.systemData.API_MajorVersion) + "." + str(self.systemData.API_MinorVersion) + "." + str(self.systemData.API_Buildnumber)
        return 0
    
    def calibrate(self):
        # ---------------------------------------------
        print "configure and start calibration"
        # ---------------------------------------------
#        self.calibrationData = CCalibration(5, 1, 0, 0, 1, 250, 220, 2, 20, b"")
        
        self.res = iViewXAPI.iV_SetupCalibration(byref(self.calibrationData))
        if self.res != 1:
            HandleError(self.res)
            return self.res
        print "iV_SetupCalibration " + str(self.res)

        self.res = iViewXAPI.iV_Calibrate()
        if self.res != 1:
            HandleError(self.res)
            return self.res
        print "iV_Calibrate " + str(self.res)
        '''
        self.res = iViewXAPI.iV_Validate()
        if self.res != 1:
            HandleError(self.res)
            return self.res
        print "iV_Validate " + str(self.res)
        
        self.res = iViewXAPI.iV_GetAccuracy(byref(self.accuracyData), 0)
        print "iV_GetAccuracy " + str(self.res)
        print "horizontal deviation target - gaze position for left eye: " + str(self.accuracyData.deviationLX) + " vertical deviation target - gaze position for left eye:" + str(self.accuracyData.deviationLY)
        print "horizontal deviation target - gaze position for right eye: " + str(self.accuracyData.deviationRX) + " vertical deviation target - gaze position for right eye: " + str(self.accuracyData.deviationRY)
        self.res = iViewXAPI.iV_ShowTrackingMonitor()
        if self.res != 1:
            HandleError(self.res)
            return self.res
        print "iV_ShowTrackingMonitor " + str(self.res)
        '''
        return self.res

    def saveCalibration(self, calibFileName):
        tmp = c_char_p(calibFileName)
        self.res = iViewXAPI.iV_SaveCalibration(calibFileName)
        return self.res
    
    def loadCalibration(self, calibFileName):
        self.res = iViewXAPI.iV_LoadCalibration(calibFileName)
        return self.res
    
    def disconnnect(self):
        print "disconnect from eye tracker"
        self.res = iViewXAPI.iV_Disconnect()
        if self.res != 1:
            HandleError(self.res)
        return self.res 
        
    def getBinocularData(self):
        self.res = iViewXAPI.iV_GetSample(byref(self.sampleData))
##        self.res = 1 ##!!!!!!!!!!!!!!!!!!!!!!!
        if self.res != 1:
#            HandleError(self.res)
            return [self.res, CSample(0, CEye(0,0,0), CEye(0,0,0),0)]  
        return self.res, self.sampleData

