'''
Class to read eye tracking data from SMI eye tracker
'''
from multiprocessing import Array, Value
import ctypes as c
import SMIApi as SMI
import time
import math
import os
import sys
import csv
import platform

# Load eye structure
from EyeStruct import *

MAX_SAMPLE_SIZE = 500

class EyeTracker():
    def __init__(self, binocular=False, scaleX=1.0, scaleY=1.0, calibPts=9, screen=None, fileName="", remoteIp="127.0.0.1", localIp="127.0.0.1"):
        self.binocular = binocular
        self.fileName  = fileName
        self.scaleX    = scaleX
        self.scaleY    = scaleY
        self.calibPts  = calibPts
        self.screen    = screen
        self.localIp   = localIp
        self.remoteIp  = remoteIp
        if fileName != "":
            print "Read eye data from file: ", fileName 
        if binocular:
            print "Return binocular data"

    def createEyeTracker(self):
        self.eyeCount   = Value('i', 0)
        self.runningT   = Value('i', 0)
        # SMI objec to to handle eye tracker
        retCode = 0
        try:
            self.smi = SMI.SMI(self.calibPts, self.screen)
        except:
            retCode = 1
        finally:
            return retCode

    def connectEyeTracker(self):
        if self.fileName != "":
            print "Opening eye data file: ", self.fileName
            try:
                f = open(self.fileName)
            except:
                print "Eye data file does not exists"
                sys.exit(1)
            self.file = csv.reader(f, delimiter=',')     
            if self.file == None:
                print "Unable to open eye data file"
                sys.exit(1)
            return 0
        elif self.fileName == "":
            print "Connect to SMI eye tracker"
            try:
                retCode = self.smi.connect("iViewXSDK_Python_SimpleExperiment.txt", self.remoteIp, self.localIp)
            except:
                retCode = 1
            finally:
                return retCode

    def calibrateEyeTracker(self):
        if self.fileName == "": 
            print "Calibrate eye tracker"
            try:
                retCode = self.smi.calibrate()
            except:
                retCode = 1
            finally:
                return retCode
        else:
            return 0
        
    def loadCalibration(self, calibFileName):
        print "Load calibration from", calibFileName
        try:
            retCode = self.smi.loadCalibration(calibFileName)
        except:
            retCode = 1
        finally:
            return retCode
    
    def saveCalibration(self, calibFileName):
        print "Save calibration to", calibFileName
        try:
            retCode = self.smi.saveCalibration(calibFileName)
        except:
            retCode = 1
        finally:
            return retCode

    def startEyeTracker(self):
        print "start reading eye data"
        #self.clearEyeTrackerData()
        self.runningT.value = 1
        return 1
    
    # =======================================================================================
    def getEyeData(self):
        retCode = 0
        retData = None
        try:
            if (self.runningT.value == 1):
                leftX = leftY = rightX = rightY = tStamp = 0
                leftPupil = rightPupil = 0.0
                if self.fileName != "": # Read eye data from a file
                    try:
                        line       =  self.file.next()
                    except:
                        print "Reached end of eye data file"
                        retCode = 1
                    else:
                        tStamp     =  long(line[0])
                        leftX      =  int(float(line[1]))
                        leftY      =  int(float(line[2]))
                        leftPupil  = float(line[3]) 
                        rightX     =  int(float(line[4]))
                        rightY     =  int(float(line[5]))
                        rightPupil = float(line[6])
                        code       = 1
                else: # Read from eye tracker
                    code, sample = self.smi.getBinocularData()
                    if (code == 1):
                        leftX  = sample.leftEye.gazeX
                        leftY  = sample.leftEye.gazeY
                        rightX = sample.rightEye.gazeX
                        rightY = sample.rightEye.gazeY
                        leftPupil  = sample.leftEye.diam
                        rightPupil = sample.rightEye.diam
                        tStamp = c.c_double(math.floor(sample.timestamp/100))
                if self.binocular:
                    retData = BothEyes(Eye(leftX*self.scaleX, leftY*self.scaleY, leftPupil, tStamp), Eye(rightX*self.scaleX, rightY*self.scaleY, rightPupil, tStamp))
                else: 
                    x = (leftX + rightX) / 2 * self.scaleX
                    y = (leftY + rightY) / 2 * self.scaleY
                    meanPupil = (leftPupil + rightPupil)/2
                    retData = Eye(x, y, meanPupil, tStamp)
            # If reading from file then sleep
            if self.fileName != "":
                time.sleep(1.0/500.0)
        except:
            retCode = 1
        finally:
            return retCode, tmpData

    def stopEyeTracker(self):
        print "stop reading from eye tracker"
        self.runningT.value = 0
        return 1
        
    def clearEyeTrackerData(self):
        #print "Clear eye tracker data"
        self.eyeData.clear()
        return 1

