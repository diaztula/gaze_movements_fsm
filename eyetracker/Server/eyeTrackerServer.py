#!/usr/bin/env python
 
"""
Antonio Diaz Tula
June, 2015

Server process that reads eye data from the eye tracker and stores in a circular queue.
This implementation works for only one client.
Uses collections.deque to mainain a circular list
"""

MAX_SAMPLE_SIZE = 500
UNDEF           = -1000000
# ----------------- Importing modules -----------------
import socket, threading, sys, os, pickle, time, argparse, importlib, platform, math
from collections import deque
from os.path import abspath, join

absPath = abspath(__file__).rsplit(os.sep, 1)[0]

# Eye structure
sys.path.append(join(absPath, "EyeTracker"))
from EyeStruct import *

# Add here the supported eye trackers
#sys.path.append(join(absPath, join("EyeTracker", "SMI"))) 

canMoveMouse = False # Will be true of the server process can move the mouse (platform dependent)
osName = platform.system()
if osName == "Windows":
    try: 
        import win32api
        canMoveMouse = True
    except:
        print "Unable to load win32api, mouse emulation wont work"
elif osName == "Linux":
    try: 
        from Xlib import X, display
        disp = display.Display() 
        root = disp.screen().root
        canMoveMouse = True
    except:
        print "Unable to load Xlib, mouse emulation wont work"

# Server class
class Server(threading.Thread):
    # =============================================================================================    
    """Init function, receives the command line arguments, creates and initializes the server"""
    def __init__(self, args):
        threading.Thread.__init__(self) 
        # Define server properties
        self.host = ''
        self.port = 9003
        self.size = 65536
        self.count = 0
        self.protocol = 1 # binary
        self.trackerDelay = 1.0 / float(args.freq)
        # Configure server socket
        try:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.sock.bind((self.host, self.port))
            self.sock.setblocking(True)
        except Exception as e:
            print "Exception while creating sockets: ", e
            sys.exit()
        self.lock = threading.Lock()
        self.args = args
        self.scaleX = float(self.args.scaleX)
        self.scaleY = float(self.args.scaleY)
        self.readEyeData = False
        self.running     = False
        # Deque 
        self.eyeDataList = deque(maxlen = MAX_SAMPLE_SIZE)

        print "Server accepting client connections on port %i"%self.port 
         
        print "<=========================================>"
        print "Eye tracker model:  ", self.args.eyeTracker
        print "Eye tracker freq:   ", self.args.freq
        print "Mouse emulation:    ", self.args.mouseEmu
        print "Binocular data:     ", self.args.binocular
        print "Provided eyeFile:   ", self.args.eyeFile
        print "Calibration points: ", self.args.calibPts
        print "Screen:             ", self.args.screen
        print "<=========================================>"
        
        if self.args.moveMouse and not canMoveMouse:
            print "Error: can not move the mouse pointer with the gaze data"
            if osName == "Windows":
                print "Package win32api is probably not installed"
            elif osName == "Linux":
                print "Package Xlib is probably not installed"
                print "You can install it with \"sudo apt-get install python-xlib\", aborting..."
            sys.exit(1)
        self.moveMouse = self.args.moveMouse
        # Currently it supports two eye trackers: SMI (remote) and Mouse (emulation)
        if self.args.mouseEmu:
            self.args.eyeTracker = "Mouse"
        try:
            sys.path.append(join( join(absPath, "EyeTracker"), self.args.eyeTracker))
            eyeTrackerModule = importlib.import_module(self.args.eyeTracker)
            self.e = eyeTrackerModule.EyeTracker( args.mouseEmu, args.binocular, float(args.scaleX), float(args.scaleY), int(args.calibPts), int(args.screen), args.eyeFile, args.remoteIp, args.localIp )
            self.e.createEyeTracker()
        except Exception as e:
            print "Error while loading and/or creating the eye tracking module: ", e
            sys.exit()
    # =============================================================================================

    def getMousePos(self):
        """Returns current mouse position IF the needed package is available.
        If not available then returns (-1, -1)
        """
        x, y = -1, -1
        if canMoveMouse:
            if osName == "Linux":
                pointer = root.query_pointer()
                data = pointer._data
                x, y = data["root_x"], data["root_y"]
            elif osName == "Windows":
                x, y = win32api.GetCursorPos()
        return x, y

    # =============================================================================================
    def run(self):
        """This methods runs as a thread that reads information from the eye tracker
        and grab the data in a circular queue.
        """
        try:
            while self.running:
                retCode, eyeData = self.e.getEyeData()
                if retCode == 0:
                    self.lock.acquire()
                    self. eyeDataList.append(eyeData)
                    self.lock.release()
                    x = eyeData.left.x
                    y = eyeData.left.y
                    if self.moveMouse and not self.args.mouseEmu:
                        self.moveMouseWithGaze(xx, yy)
                if self.args.mouseEmu:
                    time.sleep(self.trackerDelay)
        except KeyboardInterrupt:
            print "Aborting reading by keyboard interrupt"
            self.running = False
        except:
            print "\nException occurred: ", sys.exc_info()[1]
            self.running = False
    # =============================================================================================
    
    # =============================================================================================
    def moveMouseWithGaze(self, x, y):
        """Move the cursor with the gaze.
        """
        x = int(x)
        y = int(y)
        if x >= 0 and x <= self.scaleX and y >= 0 and y <= self.scaleY:
            if osName == "Linux":
                root.warp_pointer(x, y)
                disp.sync()
            elif osName == "Windows":
                win32api.SetCursorPos( (int(x), int(y)) )
    # =============================================================================================

    # =============================================================================================
    def processMsg(self, data, address):
        """Receives and process a message from the client and sends the return data via socket.
        Some of this options could not be supported by the eye tracker.
        Return value: 0 = success, 1 = error"""        
        if data == "calibrate":
            print "Calibrate the eye tracker"
            r = self.e.calibrateEyeTracker()
            if (r == 1):
                sock.sendto("success", address)
            else:
                sock.sendto("error_%i"%r, address)
                
        elif data == "saveCalibration":
            print "Save current calibration"
            self.sock.setblocking(True)
            self.sock.sendto("sendCalibFile", address)
            try:
                calibFileName, addr = self.sock.recvfrom(self.size)
                print "File name received: ", calibFileName
            except Exception as e:
                print "Exception occurred: ", e
            if not self.args.mouseEmu:
                r = self.e.saveCalibration(calibFileName)
            else:
                r = 1
            if r == 1:
                print "Calibration file saved with success"
                self.sock.sendto("success", addr)
            else:
                print "Could not save calibration file, server return code:", r
                self.sock.sendto("error_%i"%r, addr)
            self.sock.setblocking(False)

        elif data == "loadCalibration":
            print "Load existing calibration"
            self.sock.setblocking(True)
            self.sock.sendto("sendCalibFile", address)
            try:
                calibFileName, addr = self.sock.recvfrom(self.size)
                print "File name received: ", calibFileName
            except Exception as e:
                print "Exception occurred: ", e
            if not self.args.mouseEmu:
                r = self.e.loadCalibration(calibFileName)
            else:
                r = 1
            if r == 1:
                print "Calibration file loaded with success"
                self.sock.sendto("success", addr)
            else:
                print "Could not load calibration file, server return code:", r
                self.sock.sendto("error_%i"%r, addr)
            self.sock.setblocking(False)

        elif data == "connect":
            print "Received connect"
            r = self.e.connectEyeTracker()
            if (r == 0):
                self.sock.sendto(pickle.dumps("success", self.protocol), address)
            else:
                self.sock.sendto(pickle.dumps("error_%i"%r, self.protocol), address)

        elif data == "start":
            print "Start reading from eye tracker"
            self.readEyeData = True
            self.sock.sendto(pickle.dumps("success", self.protocol), address)
            self.count = 0

        elif data == "clear":
            self.lock.acquire()
            self.eyeDataList.clear()
            self.lock.release()
            self.count = 0
            self.sock.sendto(pickle.dumps("success", self.protocol), address)

        elif data == "get_data":
            try:
                self.lock.acquire()
                if len(self.eyeDataList) > 0:
                    self.sock.sendto(pickle.dumps(list(self.eyeDataList), self.protocol), address)
                    self.count += len(self.eyeDataList)
                else:
                    self.sock.sendto(pickle.dumps("no_data", self.protocol), address)
            finally: 
                self.eyeDataList.clear()
                self.lock.release()
                    
        elif data == "stop":
            print "Stop reading from eye tracker"
            self.readEyeData = False
            self.sock.sendto(pickle.dumps("success", self.protocol), address)
            print "Last client received ", self.count, "samples"
                
        elif data == "exit":
            self.running = False
    # =============================================================================================

    # =============================================================================================
    def startServer(self):
        try:
            self.running = True
            self.start()
            t0 = t1 = 0
            while self.running:
                try:
                    pass
                    data, address = self.sock.recvfrom(self.size)
                    self.processMsg(data, address)
                except KeyboardInterrupt:
                    print "Exit by Ctrl-C"
                    self.running = False
                    self.join()
                    sys.exit(1)
                except:
                    pass
            self.sock.close()
        except KeyboardInterrupt:
            print "Exit by Ctrl-C"
            self.running = False
            self.join()
            sys.exit(1)
        

if __name__ == '__main__':
    # Parse command line arguments
    parser = argparse.ArgumentParser(description = "Standalone process for reading gaze data from the eye tracker")
    parser.add_argument("--eyeTracker" , default = "SMI" , help = "Eye tracker model to communicate with: SMI (default). Ignored for mouse emulation.")  # eye tracker model    
    parser.add_argument("--freq"       , default = 500   , help = "Eye tracker frequency in Hertz (default is 500).")  # eye tracker frequency    
    parser.add_argument("--mouseEmu"   , action = "store_true", help = "Emulate the eye tracker with the mouse.", default = False)  # Emulate eye tracker with mouse
    parser.add_argument("--binocular"  , default = 1, help = "Set to 1 to return binocular eye data (default), 0 to return monocular data.") # Return data from both eyes
    parser.add_argument("--eyeFile"    , default = ""    , help = "If set, specify a file previously recorded with eye data.")
    # Adjust the width and height
    parser.add_argument("--scaleX"   , default = 1.0, help = "Scale factor of the horizontal eye coordinates (default is 1.0).")
    parser.add_argument("--scaleY"   , default = 1.0, help = "Scale factor of the vertical eye coordinates (default is 1.0).")    
    # Move the cursor pointer?
    parser.add_argument("--moveMouse", action = "store_true", default = False  , help = "Update mouse position with the latest eye sample.")
    # Number of calibration points
    parser.add_argument("--calibPts" , default = 5  , help = "Number of calibration points: 5 (default) or 9")
    # Number of calibration points
    parser.add_argument("--screen"   , default = 0  , help = "Screen number to calibrate the eye tracker (default is 0)")
    # parser.add_argument("--surfaceName"   , default = "scene"  , help = "Surface name when using the Pupil eye tracker (default is \"scene\")")
    # IP of the station running the eye tracker
    parser.add_argument("--remoteIp" , default = "127.0.0.1"  , help = "IP of the remote station running the eye tracker (default is 127.0.0.1)")
    # local IP
    parser.add_argument("--localIp"  , default = "127.0.0.1"  , help = "Local IP running this process (default is 127.0.0.1)")
    args = parser.parse_args()
    
    # Create server object
    server = Server(args)
    server.startServer()    





