import socket
import pickle
import sys
from EyeStruct import *

class EyeTrackerClient():
    """This class provides methods to read gaze data from the server process.
    Please ensure that the server process is running and that the you configured 
    the correct server IP.
    """
    def __init__(self, serverIP = "127.0.0.1", verbose = True):
        """Initializes the instance parameters.
        """
        # configure the client
        self.port = 9003
        self.server = serverIP
        self.size = 65536
        self.timeout = 8
        retCode = -1
        self.verbose = verbose
        
    def initEyeTracker(self):
        """Creates the socket and establish connection
        to the server process.
        """
        # initialize socket
        try:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.sock.setblocking(2)
            # connect to tracker
            self.sock.sendto("connect", (self.server, self.port))
            response = self.sock.recv(self.size)     
            msg = pickle.loads(response)
            if (msg == "success"):
                retCode = 1
                print "Connected to server process"
            else:
                print "Server process refused connection: ", msg
                retCode = -1
        except:
            print "Cannot connect to server (not running?)"
            retCode = -1
        finally:
            return retCode
    
    def readDataFromEyeTracker(self):
        """Reads all gaze samples from the server process. It can return no data or
        several samples, depending on the eye tracker frequency and how frequently 
        this method is called. It clears the server process queue and pull all data 
        that is returned as a list.
        """
        try:
            eyeData = []
            self.sock.sendto("get_data", (self.server, self.port))
            response = self.sock.recv(self.size)   
            #print "response = ", response
            rawData =  pickle.loads(response)
            if isinstance(rawData, list): 
                eyeData = rawData
            else:
                if self.verbose: print "No data received from server process"
        except socket.error:
            print "socket error while talking to server process"
        finally:    
            return eyeData
        
    def calibrateEyeTracker(self):
        """Requests the server to calibrate the eye tracker. Note that this 
        feature might not be available for all eye trackers. Currently it works
        for the SMI RED, thanks to its API.
        """
        retCode = -1
        try:
            self.sock.sendto("calibrate", (self.server, self.port))
            response = self.sock.recv(self.size)     
            if (response == "success"):
                if self.verbose: print "Calibration succeeded"
                retCode = 1
            else:
                if self.verbose: print "Calibration failed", retCode
                retCode = -1
        except socket.error:
            print "Server socket error while trying calibration"
            retCode = -1
        finally:    
            return retCode

    def saveCalibration(self, calibFileName):
        """Requests the server to save current calibration. Note that this 
        feature might not be available for all eye trackers. Currently it works
        for the SMI RED, thanks to its API.
        """
        retCode = -1
        try:
            self.sock.sendto("saveCalibration", (self.server, self.port))
            response = self.sock.recv(self.size)     
            if response == "sendCalibFile":
                self.sock.sendto(calibFileName, (self.server, self.port))
                response = self.sock.recv(self.size)     
                if (response == "success"):
                    if self.verbose: print "Calibration file saved with success"
                    retCode = 1
            if retCode != 1:
                if self.verbose: print "Save calibration failed"
                retCode = -1
        except socket.error:
            print "Server socket error while calibrating the eye tracker"
            retCode = -1
        finally:    
            return retCode

    def loadCalibration(self, calibFileName):
        """Requests the server to load a previously saved calibration. Note that this 
        feature might not be available for all eye trackers. Currently it works
        for the SMI RED, thanks to its API.
        """
        retCode = -1
        try:
            self.sock.sendto("loadCalibration", (self.server, self.port))
            response = self.sock.recv(self.size)     
            if response == "sendCalibFile":
                self.sock.sendto(calibFileName, (self.server, self.port))
                response = self.sock.recv(self.size)     
                if (response == "success"):
                    if self.verbose: print "Calibration file load with success"
                    retCode = 1
            if retCode != 1:
                print "Load calibration failed"
                retCode = -1
        except socket.error:
            print "Server socket error while trying calibration"
            retCode = -1
        finally:    
            return retCode

    def startEyeTracker(self):
        """Requests the server to start collecting gaze samples.
        """
        try:
            self.sock.sendto("start", (self.server, self.port))
            response = self.sock.recv(self.size)     
            msg = pickle.loads(response)
            if (msg == "success"):
                retCode = 1
            else:
                retCode = -1
        except socket.error:
            print "Cannot start eye tracker from server"
            retCode = -1
        finally:    
            return retCode

    def stopEyeTracker(self):
        """Requests the server to stop collecting gaze samples.
        """
        try:
            self.sock.sendto("stop", (self.server, self.port))
            response = self.sock.recv(self.size)     
            msg = pickle.loads(response)
            if (msg == "success"):
                retCode = 0
            else:
                retCode = 1
            print "retCode=", retCode
        except socket.error:
            print "Cannot stop eye tracker from server"
            retCode = 1
        finally:
            return retCode

    def clearEyeTrackerData(self):
        """Requests the server to clear all existing gaze samples in the queue.
        """
        try:
            self.sock.sendto("clear", (self.server, self.port))
            response = self.sock.recv(self.size)     
            msg = pickle.loads(response)
            if (msg == "success"):
                retCode = 0
            else:
                retCode = 1
        except socket.error:
            print "Cannot clear eye tracking data from server"
            retCode = 1
        finally:    
            return retCode

    # -------------------------------------------    
    def getGazeState(self):
        """Get current gaze state.
        """
        try:
            self.sock.sendto("get_gaze_state", (self.server, self.port))
            response = self.sock.recv(self.size)   
            #print "response = ", response
            rawData =  pickle.loads(response)
        except socket.error:
            print "socket error while talking to server process"
        finally:    
            return rawData
    
            
    
            
